#include "fs.h"

#define MY_MAX_PATH 100
#define MY_MAX_FILE_SIZE 10000
#define MY_MAX_CHILDREN 100

#define MY_BLOCK_SIZE 10

// struct myfcb {
// 	char path[MY_MAX_PATH]; //"/jim/bob.txt"
// 	uuid_t id;

// 	uuid_t file_data_id;

// 	// see 'man 2 stat' and 'man 2 chmod'
// 	//meta-data for the 'file'
// 	uid_t  uid;		/* user */
//     gid_t  gid;		/* group */
// 	mode_t mode;	/* protection */
// 	time_t mtime;	/* time of last modification */
// 	time_t ctime;	 time of last change to meta-data (status) 
// 	off_t size;		/* size */
	
// 	//meta-data for the root thing (directory), so every file knows where the root is?
// 	uid_t  root_uid;		/* user */
//     gid_t  root_gid;		/* group */
// 	mode_t root_mode;	/* protection */
// 	time_t root_mtime;	/* time of last modification */
// };


// "At the very least you will need to define a structure for directory contents."
struct dirent {
	char path[MY_MAX_PATH];
	uuid_t id;
};

struct myfcb {
	char path[MY_MAX_PATH];
	uuid_t file_data_id;
	uuid_t id;
	
	// see 'man 2 stat' and 'man 2 chmod'
	//meta-data for the 'file'
	uid_t  uid;		/* user */
    gid_t  gid;		/* group */
	mode_t mode;	/* protection */
	time_t mtime;	/* time of last modification */
	time_t atime;	/* time of last access */
	time_t ctime;	/* time of last change to meta-data (status) */
	off_t size;		/* size */

	uuid_t directBlockIds[12];
	uuid_t singlyIndirectBlockId;
	uuid_t doublyIndirectBlockId;
	uuid_t triplyIndirectBlockId;
	
};

//We want a thingy that splits the path up into bits at each slash (/) and traverses the tree down
//through the children.
//Alternatively it finds a number or something at each place i dunno.

//A traversal thing
int getFcb(const char *path, struct myfcb *buf);
int getFcbHelper(const char *path, struct myfcb *buf, struct myfcb current);

unqlite_int64 MY_FCBSIZE = sizeof(struct myfcb);