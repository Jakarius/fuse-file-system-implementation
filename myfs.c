/*
  MyFS. One directory, one file, 1000 bytes of storage. What more do you need?
  
  This Fuse file system is based largely on the HelloWorld example by Miklos Szeredi <miklos@szeredi.hu> (http://fuse.sourceforge.net/helloworld.html). Additional inspiration was taken from Joseph J. Pfeiffer's "Writing a FUSE Filesystem: a Tutorial" (http://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/).
*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <regex.h>

#include "myfs.h"

// The one and only fcb that this implmentation will have. We'll keep it in memory. A better 
// implementation would, at the very least, cache it's root directroy in memory. 
struct myfcb the_root_fcb;


int getParentPath(const char *path, char *buf) {

    int lastSlashPos = 0;
    for (int i = 0; i < strlen(path); i++) {
        if (path[i] == '/') {
            lastSlashPos = i;
        }
    }

    if (lastSlashPos == 0) {
        memcpy(buf, path, lastSlashPos + 1);
        buf[lastSlashPos+1] = '\0';
    } else {
        memcpy(buf, path, lastSlashPos);
        buf[lastSlashPos] = '\0';
    }
}

// Get file and directory attributes (meta-data).
// Read 'man 2 stat' and 'man 2 chmod'.
static int myfs_getattr(const char *path, struct stat *stbuf){
    

    if (strstr(path, "/.git") != NULL || strstr(path, "/HEAD") != NULL) {
        return -1;
    }

    write_log("myfs_getattr(path=\"%s\", statbuf=0x%08x)        ", path, stbuf);

    memset(stbuf, 0, sizeof(struct stat));

    struct myfcb fcbBuf;
    int result = getFcb(path, &fcbBuf);

    if (result >= 0) { //do search here, return value signifying found or not found.
        printf("mode of %s  : %o\n", path, fcbBuf.mode);
        printf("st_uid : %lu\n", fcbBuf.uid);
        stbuf->st_mode = fcbBuf.mode;
        stbuf->st_nlink = 1; //need something to keep track of these
        stbuf->st_mtime = fcbBuf.mtime;
        stbuf->st_ctime = fcbBuf.ctime;
        stbuf->st_atime = fcbBuf.atime;
        stbuf->st_size = fcbBuf.size;
        stbuf->st_uid = fcbBuf.uid;
        stbuf->st_gid = fcbBuf.gid;
    } else {
        write_log("myfs_getattr - ENOENT\n");
        return -ENOENT;
    }

    return 0;
}

// Read a directory.
// Read 'man 2 readdir'.
static int myfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    (void) offset;
    (void) fi;

    write_log("write_readdir(path=\"%s\", buf=0x%08x, filler=0x%08x, offset=%lld, fi=0x%08x)\n", path, buf, filler, offset, fi);
    
    // This implementation supports only a root directory so return an error if the path is not '/'.
    // if (strcmp(path, "/") != 0){
    //  write_log("myfs_readdir - ENOENT");
    //  return -ENOENT;
    // }

    printf("\n\nREADING DIRECTORY: %s\n", path);

    char pathWithTrailingSlash[MY_MAX_PATH];
    if (strcmp(path, "/") != 0) {
        strcpy(pathWithTrailingSlash, path);
        strcat(pathWithTrailingSlash, "/");
    }

    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    struct myfcb parentFcb;
    int traverseResult = getFcb(path, &parentFcb);

    //Get children of the thing
    printf("GETTING Children\n");
    printf("%d\n", parentFcb.uid);
    print_id(&(parentFcb.id));
    printf("\n\n");
    unqlite_int64 parentSize = parentFcb.size;
    struct dirent dirents[MY_MAX_CHILDREN];
    memset(dirents, 0, MY_MAX_CHILDREN*sizeof(struct dirent));
    unqlite_kv_fetch(pDb, parentFcb.file_data_id, KEY_SIZE, &dirents, &parentSize);

    for (int i = 0; i < parentSize / sizeof(struct dirent); i++) {
        char *pathP = (char*)&(dirents[i].path);

        if(*pathP!='\0'){
            // drop the leading path up to the last'/';

            int lastSlashPos = 0;
            for (int i = 0; i < strlen(pathP); i++) {
                if (pathP[i] == '/') {
                    lastSlashPos = i;
                }
            }

            pathP+=lastSlashPos+1;
            printf("PATH P IN READDIR: %s\n", pathP);
            int result = filler(buf, pathP, NULL, 0);
            printf("readdir result: %d\n", result);
            if (result != 0) {
                return 0;
            }
        }
    }


    printf("ENDING READDIR\n");
    
    return 0;
}


// Read a file.
// Read 'man 2 read'.
static int myfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    size_t len;
    (void) fi;
    
    write_log("myfs_read(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n", path, buf, size, offset, fi);
    
    struct myfcb fcb; 
    int traverseResult = getFcb(path, &fcb);

    if (traverseResult < 0) {
        write_log("myfs_read - Unable to get fcb for given path, ENOENT");
        return -ENOENT;
    }

    len = fcb.size;
    
    uint8_t data_block[MY_MAX_FILE_SIZE];
    
    memset(data_block, 0, MY_MAX_FILE_SIZE);
    // Is there a data block?
    if(uuid_compare(zero_uuid,fcb.file_data_id)!=0){
        unqlite_int64 nBytes;  //Data length.
        int rc = unqlite_kv_fetch(pDb,fcb.file_data_id,KEY_SIZE,NULL,&nBytes);
        if( rc != UNQLITE_OK ){
          error_handler(rc);
        }
        if(nBytes>MY_MAX_FILE_SIZE){
            write_log("myfs_read - EIO");
            return -EIO;
        }
    
        // Fetch the file data for this fcb.
        unqlite_kv_fetch(pDb,fcb.file_data_id,KEY_SIZE,data_block,&nBytes);
    }
    
    if (offset < len) {
        if (offset + size > len) {
            size = len - offset;
        }
        memcpy(buf, data_block + offset, size);
    } else
        size = 0;

    return size;
}

// Path must be '/<something>'.
// Read 'man 2 creat'.
static int myfs_create(const char *path, mode_t mode, struct fuse_file_info *fi){   
    write_log("myfs_create(path=\"%s\", mode=0%03o, fi=0x%08x)\n", path, mode, fi);
        
    printf("CREATIN\n");
    int pathlen = strlen(path);
    if(pathlen>=MY_MAX_PATH){
        write_log("myfs_create - ENAMETOOLONG");
        return -ENAMETOOLONG;
    }

    char pathOfParent[MY_MAX_PATH];
    getParentPath(path, pathOfParent);

    printf("PATH: %s\n", path);
    printf("PATH OF PARENT: %s\n", pathOfParent);
    //Need to get the fcb of the path of the parent so we can connect them.
    struct myfcb parentFcb; 
    int traverseResult = getFcb(pathOfParent, &parentFcb);

    if (traverseResult < 0) {
        write_log("myfs_create - Couldn't get fcb for given path=\"%s\", ENOENT", pathOfParent);
        return -ENOENT;
    }

    unqlite_int64 parentSize = parentFcb.size;
    struct dirent parentDirents[MY_MAX_CHILDREN] = { 0 };

    if (parentFcb.size == 0 || uuid_compare(zero_uuid, parentFcb.file_data_id) == 0) {
        //Generate a file data id for the parent fcb, which currently has no dirent children.
        uuid_generate(parentFcb.file_data_id);
        printf("Generated a file data id for the root fcb\n");
        print_id(&parentFcb.file_data_id);
        printf("\n\n");
    } else {
        //Modify the existing parent FCB data block to include an extra dirent.
        unqlite_kv_fetch(pDb, parentFcb.file_data_id, KEY_SIZE, parentDirents, &parentSize);

        printf("DIRENT LIST OF Parent FCB: \n");
        for (int i = 0; i < parentSize / sizeof(struct dirent); i++) {
            printf("    %s      ", parentDirents[i].path);
            print_id(&parentDirents[i].id);
            printf("\n");
        }
    }


    //We got the parent dirents (if there were any), now add a new one.
    struct dirent newDirent;
    printf("COPYING PATH TO NEW DIRENT %s    size: %d\n", path, sizeof(newDirent.path));
    strncpy(newDirent.path, path, MY_MAX_PATH);
    printf("PATH SHOULD HAVE COPIED: %s\n", newDirent.path);
    uuid_generate(newDirent.id);

    //Add the new dirent to the parent fcbs dirents.
    printf("parentSize: %d\n", parentSize);
    memcpy(parentDirents + (parentSize / sizeof(struct dirent)), &newDirent, sizeof(struct dirent));

    //Parent dirents now updated with a new one so restore it.
    parentSize += sizeof(struct dirent);
    printf("BLEIIP %d\n", parentSize);
    int rc = unqlite_kv_store(pDb, parentFcb.file_data_id,KEY_SIZE, parentDirents, parentSize);
    // int rc = unqlite_kv_store(pDb, parentFcb.file_data_id,KEY_SIZE, parentDirents, MY_MAX_FILE_SIZE);

    struct dirent tester[MY_MAX_CHILDREN];
    unqlite_int64 asd;
    unqlite_kv_fetch(pDb, parentFcb.file_data_id, KEY_SIZE, NULL, &asd);
    unqlite_kv_fetch(pDb, parentFcb.file_data_id, KEY_SIZE, tester, &asd);

    for (int i = 0; i < parentSize / sizeof(struct dirent); i++) {
        printf("%s\n", tester[i].path);
        print_id(&tester[i].id);
        printf("\n\n");
    }
    printf("\n\n");


    //Update parent fcb size which is now one sizeof(struct dirent) larger.
    parentFcb.size = parentSize;

    //Workaround since root_fcb is no longer pointed to in any way.
    if (strcmp(pathOfParent, "/") == 0) {
        the_root_fcb.size = parentSize;
    }

    printf("\n\n\nTEST ROOT FCB SIZE  %d       %d\n ", parentFcb.size, the_root_fcb.size);
    printf("The root fcb id\n");
    print_id(&the_root_fcb.id);
    printf("\n");

    printf("\n\nParent FCB id, should be same as roots in the first case. \n");
    print_id(&parentFcb.id);
    printf("\n");
    printf("File data id: \n");
    print_id(&parentFcb.file_data_id);
    printf("\n\n");
    unqlite_kv_store(pDb, parentFcb.id, KEY_SIZE, &parentFcb, MY_FCBSIZE);

    printf("After storing the parentFcb at what should be the root_fcb's id, check if its file data id matches the roots.\n");
    print_id(&the_root_fcb.file_data_id);
    printf("\n");

    //Create a new fcb and store it using the id of the new dirent as the database key.
    struct myfcb newFcb;
    memset(&newFcb, 0, MY_FCBSIZE);

    sprintf(newFcb.path,path);
    struct fuse_context *context = fuse_get_context();
    uuid_copy(newFcb.id, newDirent.id);
    newFcb.uid=context->uid;
    newFcb.gid=context->gid;
    time_t now = time(NULL);
    newFcb.mtime = now;
    newFcb.ctime = now;
    newFcb.atime = now;

    printf("MODE: %o\n", mode);

    if (S_ISDIR(mode)) {
        newFcb.mode=mode & ~S_IFREG;
    } else {
        newFcb.mode=mode|S_IFREG;
    }


    // drw-rw-r--    =    100644
    printf("\nTHE NEW FILE MODE %o\n", newFcb.mode);

    printf("\nTHIS IS THE ID OF THE NEW DIRENT\n");
    print_id(&(newDirent.id));
    printf("Newly created file's path       %s\n\n", newDirent.path);
    
    rc = unqlite_kv_store(pDb, newDirent.id, KEY_SIZE, &newFcb,MY_FCBSIZE);
    if( rc != UNQLITE_OK ){
        write_log("myfs_create - EIO");
        return -EIO;
    }
    write_log("myfs_create - Created %s\n", path);
    return 0;
}

// Set update the times (actime, modtime) for a file. This FS only supports modtime.
// Read 'man 2 utime'.
static int myfs_utime(const char *path, struct utimbuf *ubuf){
    write_log("myfs_utime(path=\"%s\", ubuf=0x%08x)\n", path, ubuf);
    
    struct myfcb fcb;
    int traverseResult = getFcb(path, &fcb);
    if (traverseResult < 0) {
        write_log("myfs_utime - Couldn't get fcb for given path, ENOENT");
        return -ENOENT;
    }

    fcb.mtime=ubuf->modtime;
    fcb.atime=ubuf->actime;
    
    // Write the fcb to the store.
    int rc = unqlite_kv_store(pDb,fcb.id,KEY_SIZE, &fcb,MY_FCBSIZE);
    if( rc != UNQLITE_OK ){
        write_log("myfs_write - EIO");
        return -EIO;
    }
    
    return 0;
}

// Write to a file.
// Read 'man 2 write'
static int myfs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){   
    write_log("myfs_write(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n", path, buf, size, offset, fi);

    if(size >= MY_MAX_FILE_SIZE){
        write_log("myfs_write - EFBIG");
        return -EFBIG;
    }

    struct myfcb fcbBuf;
    int traverseResult = getFcb(path, &fcbBuf);
    if (traverseResult < 0) {
        write_log("myfs_write - Couldn't get fcb for given path=\"%s\", ENOENT");
        return -ENOENT;
    }

    uint8_t data_block[MY_MAX_FILE_SIZE];
    
    memset(data_block, 0, MY_MAX_FILE_SIZE);
    // Is there a data block?
    if (uuid_compare(zero_uuid,fcbBuf.file_data_id)==0) {
        // Generate a UUID for the data block. We'll write the block itself later.
        uuid_generate(fcbBuf.file_data_id);
    } else {
        // First we will check the size of the object in the store to ensure that we won't overflow the buffer.
        unqlite_int64 nBytes;  // Data length.
        int rc = unqlite_kv_fetch(pDb,fcbBuf.file_data_id,KEY_SIZE,NULL,&nBytes);
        printf("\n\n\n");
        print_id(&fcbBuf.file_data_id);
        printf("\n\n\n");
        if (rc!=UNQLITE_OK || nBytes > MY_MAX_FILE_SIZE){
            write_log("error writing: %d", nBytes);
            write_log("myfs_write - EIO reading existing data block");
            return -EIO;
        }
    
        // Fetch the data block from the store. 
        unqlite_kv_fetch(pDb,fcbBuf.file_data_id,KEY_SIZE,data_block,&nBytes);
        // Error handling?
    }
    
    // Write the data in-memory.
    int written = 0;
    if (fcbBuf.size + size +1 <= MY_MAX_FILE_SIZE) { // +1 included for null terminator.
        written = snprintf(data_block, size+1, buf);
        printf("\"written\" value %d\n", written);    
    }
    
    // Write the data block to the store.
    int rc = unqlite_kv_store(pDb,fcbBuf.file_data_id,KEY_SIZE,data_block,fcbBuf.size+written);
    if (rc != UNQLITE_OK ){
        write_log("myfs_write - EIO writing block");
        return -EIO;
    }
    
    // Update the fcb in-memory.
    fcbBuf.size+=written;
    time_t now = time(NULL);
    fcbBuf.mtime=now;
    fcbBuf.ctime=now;
    fcbBuf.atime=now;
    
    // Write the fcb to the store.
    rc = unqlite_kv_store(pDb,fcbBuf.id,KEY_SIZE,&fcbBuf,MY_FCBSIZE);
    if (rc != UNQLITE_OK ){
        write_log("myfs_write - EIO");
        return -EIO;
    }
    
    return written;
}

// Set the size of a file.
// Read 'man 2 truncate'.
int myfs_truncate(const char *path, off_t newsize){    
    write_log("myfs_truncate(path=\"%s\", newsize=%lld)\n", path, newsize);
    
    if (newsize >= MY_MAX_FILE_SIZE){
        write_log("myfs_truncate - EFBIG");
        return -EFBIG;
    }
    
    struct myfcb fcbBuf;
    int traverseResult = getFcb(path, &fcbBuf);
    if (traverseResult < 0) {
        write_log("myfs_write - Couldn't get fcb for given path=\"%s\", ENOENT");
        return -ENOENT;
    }

    fcbBuf.size = newsize;
    time_t now = time(NULL);
    fcbBuf.mtime=now;
    fcbBuf.ctime=now;
    
    // Write the fcb to the store.
    int rc = unqlite_kv_store(pDb, fcbBuf.id, KEY_SIZE, &fcbBuf, MY_FCBSIZE);
    if( rc != UNQLITE_OK ){
        write_log("myfs_write - EIO");
        return -EIO;
    }
    
    return 0;
}

// Set permissions.
// Read 'man 2 chmod'.
int myfs_chmod(const char *path, mode_t mode){
    write_log("myfs_chmod(fpath=\"%s\", mode=0%03o)\n", path, mode);
    
    struct myfcb fcbBuf;
    getFcb(path, &fcbBuf);

    printf("CHANGING MOD %o\n", mode);

    fcbBuf.mode = mode;
    time_t now = time(NULL);
    fcbBuf.ctime = now;

    int rc = unqlite_kv_store(pDb, fcbBuf.id, KEY_SIZE, &fcbBuf, MY_FCBSIZE);
    if (rc != UNQLITE_OK) {
        write_log("myfs_chmod - EIO");
        error_handler(rc);
        return -1;
    } else {
        return 0;
    }
}

// Set ownership.
// Read 'man 2 chown'.
int myfs_chown(const char *path, uid_t uid, gid_t gid) {   
    write_log("myfs_chown(path=\"%s\", uid=%d, gid=%d)\n", path, uid, gid);

    struct myfcb fcbBuf;
    int traverseResult = getFcb(path, &fcbBuf);
    if (traverseResult < 0) {
        write_log("Couldn't find fcb from given path=\"%s\", ENOENT", path);
        return -ENOENT;
    }

    if (uid != -1) {
     fcbBuf.uid = uid;
    }
    if (gid != -1) {
     fcbBuf.gid = gid;
    }

    if (errno == EPERM) {
     return -EPERM;
    }

    int rc = unqlite_kv_store(pDb, fcbBuf.id, KEY_SIZE, &fcbBuf, MY_FCBSIZE);
    if (rc != UNQLITE_OK) {
        error_handler(rc);
        return -1;
    }

    return 0;
}

// Create a directory.
// Read 'man 2 mkdir'.
int myfs_mkdir(const char *path, mode_t mode){
    write_log("myfs_mkdir: %s\n",path); 

    mode |= S_IFDIR|S_IRUSR|S_IWUSR|S_IRGRP; 
    return myfs_create(path, mode, NULL);
}

// Delete a file.
// Read 'man 2 unlink'.
int myfs_unlink(const char *path){
    write_log("myfs_unlink: %s\n",path);

    struct myfcb fcbBuf;
    int traverseResult = getFcb(path, &fcbBuf);
    if (traverseResult < 0) {
        write_log("Couldn't find fcb from given path=\"%s\", ENOENT", path);
        return -ENOENT;
    }

    if (S_ISDIR(fcbBuf.mode)) {
        write_log("myfs_unlink - Cannot delete a directory with unlink.");
        return -1;
    }

    unqlite_int64 size;
    int rc = unqlite_kv_fetch(pDb, fcbBuf.file_data_id, KEY_SIZE, NULL, &size);

    //get parent fcb.
    char parentPath[MY_MAX_PATH];
    getParentPath(path, parentPath);

    struct myfcb parentFcbBuf;
    traverseResult = getFcb(parentPath, &parentFcbBuf);
    if (traverseResult < 0) {
        write_log("Couldn't find fcb from given path=\"%s\", ENOENT", path);
        return -ENOENT;
    }

    //Get the directory entries for the parent fcb in order to remove the desired one.
    unqlite_kv_fetch(pDb, parentFcbBuf.file_data_id, KEY_SIZE, NULL, &size);

    struct dirent parentDirents[MY_MAX_CHILDREN] = { 0 };

    rc = unqlite_kv_fetch(pDb, parentFcbBuf.file_data_id, KEY_SIZE, &parentDirents, &size);
    if (rc != UNQLITE_OK) {
        printf("UNQLITE Error while fetching dirents from parent fcb.\n");
        error_handler(rc);
    }

    //Delete the desired fcb from the database.
    rc = unqlite_kv_delete(pDb, fcbBuf.id, KEY_SIZE);
    if (rc != UNQLITE_OK){
        error_handler(rc);
        return -1;
    }

    //Update the parent fcb with a reduced size;
    printf("myfs_unlink, reducing size of parent fcb from %d to %d\n", parentFcbBuf.size, parentFcbBuf.size - sizeof(struct dirent));
    parentFcbBuf.size -= sizeof(struct dirent);
    rc = unqlite_kv_store(pDb, parentFcbBuf.id, KEY_SIZE, &parentFcbBuf, MY_FCBSIZE);
    if (rc != UNQLITE_OK) {
        error_handler(rc);
    }

    //Delete the parent fcbs dirents so that they can be re-added minus the deleted one.
    //Avoids fragmentation in the data structure.

    rc = unqlite_kv_delete(pDb, parentFcbBuf.file_data_id, KEY_SIZE);
    if (rc != UNQLITE_OK) {
        printf("UNQLITE Error while deleting parent fcb's dirents.\n");
        error_handler(rc);
    }

    struct dirent newParentDirents[MY_MAX_CHILDREN] = { 0 };
    int inserted = 0;
    for (int i = 0; i < size / sizeof(struct dirent); i++) {

        if (uuid_compare(zero_uuid, parentDirents[i].id) != 0 && strcmp(path, parentDirents[i].path) != 0) {
            
            newParentDirents[inserted] = parentDirents[i];
            printf("PATH OF INSERTED: %s\n", parentDirents[inserted].path);
            print_id(&newParentDirents[inserted].id);
            printf("\n\n");

            inserted++;
        }
    }

    //Some inconsistency in the data base entry size after removing a directory.

    rc = unqlite_kv_store(pDb, parentFcbBuf.file_data_id, KEY_SIZE, &newParentDirents, size - sizeof(struct dirent));
    if (rc != UNQLITE_OK) {
        error_handler(rc);
    }

    return 0;
}

// Delete a directory.
// Read 'man 2 rmdir'.
int myfs_rmdir(const char *path){
    write_log("myfs_rmdir: %s\n",path); 
    
    struct myfcb fcbBuf;
    int traverseResult = getFcb(path, &fcbBuf);
    if (traverseResult < 0) {
        write_log("Couldn't find fcb from given path=\"%s\", ENOENT", path);
        return -ENOENT;
    }

    //Check that this fcb has no child dirents
    unqlite_int64 size;
    int rc = unqlite_kv_fetch(pDb, fcbBuf.file_data_id, KEY_SIZE, NULL, &size);

    if (fcbBuf.size != 0) {  //NEED TO FIND A WAY TO GET DIRECTORIES TO COUNT AS EMPTY ONCE THEIR ENTRIES HAVE BEEN UNLINKED.
        printf("myfs_rmdir - requested directory is not empty.\n", path);
        error_handler(rc);
        return -1;
    }

    //get parent fcb and remove fcb at "path" from children.
    char parentPath[MY_MAX_PATH];
    getParentPath(path, parentPath);

    struct myfcb parentFcbBuf;
    traverseResult = getFcb(parentPath, &parentFcbBuf);
    if (traverseResult < 0) {
        write_log("Couldn't find fcb from given path=\"%s\", ENOENT", path);
        return -ENOENT;
    }

    //Get the directory entries for the parent fcb in order to remove the desired one.
    unqlite_kv_fetch(pDb, parentFcbBuf.file_data_id, KEY_SIZE, NULL, &size);

    struct dirent parentDirents[MY_MAX_CHILDREN] = { 0 };
    // memset(parentDirents, 0, MY_MAX_CHILDREN * sizeof(struct dirent));

    rc = unqlite_kv_fetch(pDb, parentFcbBuf.file_data_id, KEY_SIZE, &parentDirents, &size);
    if (rc != UNQLITE_OK) {
        printf("UNQLITE Error while fetching dirents from parent fcb.\n");
        error_handler(rc);
    }

    //Delete the desired fcb from the database.
    rc = unqlite_kv_delete(pDb, fcbBuf.id, KEY_SIZE);
    if (rc != UNQLITE_OK){
        error_handler(rc);
        return -1;
    }

    //Update the parent fcb with a reduced size;
    printf("myfs_rmdir, reducing size of parent fcb from %d to %d\n", parentFcbBuf.size, parentFcbBuf.size - sizeof(struct dirent));
    parentFcbBuf.size -= sizeof(struct dirent);
    rc = unqlite_kv_store(pDb, parentFcbBuf.id, KEY_SIZE, &parentFcbBuf, MY_FCBSIZE);
    if (rc != UNQLITE_OK) {
        error_handler(rc);
    }

    //Delete the parent fcbs dirents so that they can be re-added minus the deleted one.
    //Avoids fragmentation in the data structure.

    //Not sure about this one
    rc = unqlite_kv_delete(pDb, parentFcbBuf.file_data_id, KEY_SIZE);
    if (rc != UNQLITE_OK) {
        printf("UNQLITE Error while deleting parent fcb's dirents.\n");
        error_handler(rc);
    }

    struct dirent newParentDirents[MY_MAX_CHILDREN] = { 0 };
    int inserted = 0;
    for (int i = 0; i < size / sizeof(struct dirent); i++) {

        if (uuid_compare(zero_uuid, parentDirents[i].id) != 0 && strcmp(path, parentDirents[i].path) != 0) {
            
            newParentDirents[inserted] = parentDirents[i];
            printf("PATH OF INSERTED: %s\n", parentDirents[inserted].path);
            print_id(&newParentDirents[inserted].id);
            printf("\n\n");

            inserted++;
        }
    }

    //Some inconsistency in the data base entry size after removing a directory.

    rc = unqlite_kv_store(pDb, parentFcbBuf.file_data_id, KEY_SIZE, &newParentDirents, size - sizeof(struct dirent));
    // rc = unqlite_kv_store(pDb, parentFcbBuf.file_data_id, KEY_SIZE, &newParentDirents, MY_MAX_FILE_SIZE);
    if (rc != UNQLITE_OK) {
        error_handler(rc);
    }

    return 0;
}

// OPTIONAL - included as an example
// Flush any cached data.
int myfs_flush(const char *path, struct fuse_file_info *fi){
    int retstat = 0;
    
    write_log("myfs_flush(path=\"%s\", fi=0x%08x)\n", path, fi);
    
    return retstat;
}

// OPTIONAL - included as an example
// Release the file. There will be one call to release for each call to open.
int myfs_release(const char *path, struct fuse_file_info *fi){
    int retstat = 0;
    
    write_log("myfs_release(path=\"%s\", fi=0x%08x)\n", path, fi);
    
    return retstat;
}

// OPTIONAL - included as an example
// Open a file. Open should check if the operation is permitted for the given flags (fi->flags).
// Read 'man 2 open'.
static int myfs_open(const char *path, struct fuse_file_info *fi){
    // if (strcmp(path, the_root_fcb.path) != 0)
    //  return -ENOENT;
        
    write_log("myfs_open(path\"%s\", fi=0x%08x)\n", path, fi);
    
    //return -EACCES if the access is not permitted.

    return 0;
}

static struct fuse_operations myfs_oper = {
    .getattr    = myfs_getattr,
    .readdir    = myfs_readdir,
    .open       = myfs_open,
    .read       = myfs_read,
    .create     = myfs_create,
    .utime      = myfs_utime,
    .write      = myfs_write,
    .truncate   = myfs_truncate,
    .flush      = myfs_flush,
    .release    = myfs_release,
    .mkdir      = myfs_mkdir,
    .chmod      = myfs_chmod,
    .chown      = myfs_chown,
    .rmdir      = myfs_rmdir,
    .unlink     = myfs_unlink,
};

int getFcb(const char *path, struct myfcb *buf) {

    unqlite_int64 fcbSize = MY_FCBSIZE;
    if (strcmp(path, "/") == 0) {
        printf("FOUND ROOT FCB\n");
        int rc = unqlite_kv_fetch(pDb, the_root_fcb.id, KEY_SIZE, buf, &fcbSize);
        if (rc != UNQLITE_OK) {
            error_handler(rc);
            return -1;
        } else {
            return 0;
        }
    } else {
        printf("\n\nSTARTING A BIG OL SEARCH for %s\n", path);
        struct myfcb rootBuf;
        unqlite_kv_fetch(pDb, the_root_fcb.id, KEY_SIZE, &rootBuf, &fcbSize);
        return getFcbHelper(path, buf, rootBuf);
    }
}

int getFcbHelper(const char *path, struct myfcb *buf, struct myfcb current) {

    if (!S_ISDIR(current.mode)) {
        printf("CURRENT PATH: %s,    %d\n", path, current.mode|S_IFDIR);
        return -1;
    }
    // current starts as the root fcb.
    //Extract data size
    unqlite_int64 size;
    int rc = unqlite_kv_fetch(pDb, current.file_data_id, KEY_SIZE, NULL, &size);
    struct dirent dirents[MY_MAX_CHILDREN] = { 0 };
    // memset(&dirents, 0, sizeof(struct dirent));

    printf("Fetching dirents from fcb with id: \n");
    print_id(&(current.id));
    printf("\n");
    printf("About to fetch dirents for this fcb using its file_data_id\n");
    print_id(&current.file_data_id);
    printf("\n");
    rc = unqlite_kv_fetch(pDb, current.file_data_id, KEY_SIZE, &dirents, &size);

    if (rc != UNQLITE_OK) {
        printf("getFcb() - no dirents fetched.\n");
        return -1;
    }

    // printf("DIRENT LIST OF CURRENT FCB: \n");
    // for (int i = 0; i < size / sizeof(struct dirent); i++) {
    //     printf("    %s      ", dirents[i].path);
    //     print_id(&dirents[i].id);
    //     printf("\n");
    // }

    if (uuid_compare(current.id, dirents[0].id) == 0) {
        printf("Current FCBS id was equal to the id of its own first child, LOOP\n");
        return -1;
    }

    printf("\nSIZE: %d\n", size);
    for (int i = 0; i < size / sizeof(struct dirent); i++) {
        printf("i: %d  DIR PATH: %s\n", i, dirents[i].path);
        printf("Searching dirent with following id\n");
        print_id(&(dirents[i].id));

        printf("search path: %s    actual: %s\n", path, dirents[i].path);
        if (strcmp(path, dirents[i].path) == 0) {
            printf("FETCHING FCB BY ID FROM DIRENT WITH PATH ---> %s\n", path);
            
            unqlite_int64 fcbSize = MY_FCBSIZE;
            int rc = unqlite_kv_fetch(pDb, dirents[i].id, KEY_SIZE, buf, &fcbSize);
            if (rc != UNQLITE_OK){
                error_handler(rc);
                return -1;
            } else {
                printf("FILE DATA ID OF FETCHED FCB\n");
                print_id(&buf->file_data_id);
                printf("\n\n\n\n");
                return 0;
            }
        }

        char searchPath[MY_MAX_PATH];
        strcpy(searchPath, dirents[i].path);
        strcat(searchPath, "/");
        printf("SEARCH PATH IN GETFCB: %s\n", searchPath);
        int beep = strncmp(searchPath, path, strlen(searchPath));
        printf("strlen(searchPath)  %d\n", strlen(searchPath));
        printf("strncmp result: %d\n", beep);
        if (strncmp(searchPath, path, strlen(searchPath)) == 0) {

            unqlite_int64 fcbSize = MY_FCBSIZE;
            struct myfcb newCurrent;
            int rc = unqlite_kv_fetch(pDb, dirents[i].id, KEY_SIZE, &newCurrent, &fcbSize);
            if (rc != UNQLITE_OK){
                error_handler(rc);
                return -1;
            } else {
                printf("ID of the FCB which should be the representing dirents[i].path=%s\n", dirents[i].path);
                print_id(&newCurrent.id);
                printf("\n");
                printf("file_data_id of the new search fcb\n");
                print_id(&newCurrent.file_data_id);
                printf("\n");

                printf("Going down to next directory level\n\n");
                return getFcbHelper(path, buf, newCurrent);
            }
        }
    }

    //Could it be referencing the '.' directory thus giving itself its own id?

    //THOUGHT STUFF
    // what we are searching for "/a/b/c"
    // dirent path found "/a"
    // what to check for in search path, letting us know to descend = "/a/" = "/a" + "/"

    // dirent path never longer than search path

    // "/a" is a substring of "/a/b/c"
    // "/b" is also a substring of "/a/b/c" but not one from the start.
    // We want to check if the start of dirent path matches search path up to a point.

    // "/apple/b" + "/" have to add on slash in case     "/apple/basket/c"
    // "/a"           "/a/b/c"

    // printf("DATA: %d\n", dirents);
    //Search for paths, if path matches then return this fcb.
    // printf("%d\n", current->size);
    // for (int i = 0; i < current->size; i+=sizeof(struct dirent)) {   ///////////Probably change this to list of dirents struct for the structure.
    //  struct dirent *d = malloc(sizeof(struct dirent));
    //  printf("DID WE GET IT\n");
    //  memcpy(d, dirents[i], sizeof(struct dirent));
        
    // }

    // //Read another fcb from the ids given by the dirents of current fcb.
    // //Basically descending a level.
    // for (int i = 0; i < current->size; i+=sizeof(struct dirent)) {
    //  struct dirent d;
    //  memcpy(&d, dirents[i], sizeof(struct dirent));
    //  unqlite_kv_fetch(pDb, &(d.id), KEY_SIZE, current, MY_FCBSIZE);
    //  return getFcbHelper(path, current);
    // }

    write_log("getFcb(path\"%s\") FILE NOT FOUND\n", path);
    printf("\n\nENDING A BIG OL SEARCH for %s IN FAILURE\n", path);
    return -1;
    // return NULL;
}


// Initialise the in-memory data structures from the store. If the root object (from the store) is empty then create a root fcb (directory)
// and write it to the store. Note that this code is executed outide of fuse. If there is a failure then we have failed toi initlaise the 
// file system so exit with an error code.
void init_fs(){
    int rc;
    printf("init_fs\n");
    //Initialise the store.
    init_store();
    if(!root_is_empty){
        printf("init_fs: root is not empty\n");
    
        unqlite_int64 nBytes;  //Data length.
        rc = unqlite_kv_fetch(pDb,root_object.id,KEY_SIZE,NULL,&nBytes);
        if( rc != UNQLITE_OK ){
          error_handler(rc);
        }
        if(nBytes!=MY_FCBSIZE){
            printf("Data object has unexpected size. Doing nothing.\n");
            exit(-1);
        }
    
        //Fetch the fcb that the root object points at. We will probably need it.
        unqlite_kv_fetch(pDb,root_object.id,KEY_SIZE,&the_root_fcb,&nBytes);

        printf("ROOT FCB SIZE ON INITIALISE: %d\n", the_root_fcb.size);
        printf("ROOT FCB FILE DATA ID ON INITALISE\n");
        print_id(&the_root_fcb.file_data_id);
        printf("\n\n");
    }else{
        printf("init_fs: root is empty\n");
        //Initialise and store an empty root fcb.
        memset(&the_root_fcb, 0, MY_FCBSIZE);
                
        //See 'man 2 stat' and 'man 2 chmod'.
        uuid_copy(the_root_fcb.id, root_object.id);
        the_root_fcb.mode |= S_IFDIR|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH; 
        the_root_fcb.mtime = time(0);
        the_root_fcb.uid = getuid();
        the_root_fcb.gid = getgid();
        // uuid_generate(the_root_fcb.file_data_id);
        
        //Generate a key for the_root_fcb and update the root object.
        uuid_generate(root_object.id);
        uuid_copy(the_root_fcb.id, root_object.id);
        
        printf("init_fs: writing root fcb\n");
        rc = unqlite_kv_store(pDb,root_object.id,KEY_SIZE,&the_root_fcb,MY_FCBSIZE);
        if( rc != UNQLITE_OK ){
            error_handler(rc);
        }
        
        printf("init_fs: writing updated root object\n");
        //Store root object.
        rc = write_root();
        if( rc != UNQLITE_OK ){
            error_handler(rc);
        }
    }
}

void shutdown_fs(){
    unqlite_close(pDb);
}

int main(int argc, char *argv[]){   
    int fuserc;
    struct myfs_state *myfs_internal_state;

    //Setup the log file and store the FILE* in the private data object for the file system.    
    myfs_internal_state = malloc(sizeof(struct myfs_state));
    myfs_internal_state->logfile = init_log_file();
    
    //Initialise the file system. This is being done outside of fuse for ease of debugging.
    init_fs();
        
    fuserc = fuse_main(argc, argv, &myfs_oper, myfs_internal_state);
    
    //Shutdown the file system.
    shutdown_fs();
    
    return fuserc;
}

// Read a directory.
// Read 'man 2 readdir'.
// static int myfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
//  (void) offset;
//  (void) fi;

//  write_log("write_readdir(path=\"%s\", buf=0x%08x, filler=0x%08x, offset=%lld, fi=0x%08x)\n", path, buf, filler, offset, fi);
    
//  // This implementation supports only a root directory so return an error if the path is not '/'.
//  if (strcmp(path, "/") != 0){
//      write_log("myfs_readdir - ENOENT");
//      return -ENOENT;
//  }

//  filler(buf, ".", NULL, 0);
//  filler(buf, "..", NULL, 0);


//  uuid_t *dataid = &(root_object.id);
    
//  unqlite_int64 nBytes;  // Data length
//  int rc = unqlite_kv_fetch(pDb,dataid,KEY_SIZE,NULL,&nBytes);
//  if( rc != UNQLITE_OK ){
//    error_handler(rc);
//  }
//  if(nBytes!=MY_FCBSIZE){
//      printf("Data object has unexpected size. Doing nothing.\n");
//      exit(-1);
//  }

//  uint8_t data_block[MY_MAX_FILE_SIZE];
//  memset(&data_block, 0, MY_MAX_FILE_SIZE);
//  unqlite_kv_fetch(pDb,&(the_root_fcb.file_data_id),KEY_SIZE,&data_block,&nBytes);

//  for (int i = 0; i < nBytes; i+=sizeof(struct dirent)) {
//      struct dirent *d = malloc(sizeof(struct dirent));
//      memcpy(d, data_block[i], sizeof(struct dirent));
//      char *pathP = malloc(sizeof(MY_MAX_PATH));
//      strcpy(pathP,d->path); //Careful of null terminator here might need extra byte.
//      if(*pathP!='\0'){
//          // drop the leading '/';
//          pathP++;
//          filler(buf, pathP, NULL, 0);
//      }
//      free(pathP);
//      free(d);
//  }

//  // char *pathP = (char*)&(the_root_fcb.path);
    
//  return 0;
// }